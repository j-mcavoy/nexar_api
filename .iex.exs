alias NexarApi.{Auth, Env}

query = """
query PartSearch {
  supSearch(
    q: "5000PF"
    filters: {manufacturer_id: ["6645", "87", "196"]}
    limit: 1
  ) {
    hits
    results {
      part {
        mpn
        manufacturer {
          name
          id
        }
        medianPrice1000 {
          price
          currency
          quantity
        }
        sellers(authorizedOnly: false) {
          company {
            name
            homepageUrl
          }
          isAuthorized
          offers {
            clickUrl
            inventoryLevel
            moq
            packaging
          }
        }
      }
    }
  }
}
"""
