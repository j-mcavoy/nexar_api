defmodule NexarApiTest do
  use ExUnit.Case
  doctest NexarApi

  test "query works" do
    query = """
    query PartSearch {
      supSearch(
        q: "5000PF"
        filters: {manufacturer_id: ["6645", "87", "196"]}
        limit: 1
      ) {
        hits
        results {
          part {
            mpn
            manufacturer {
              name
              id
            }
            medianPrice1000 {
              price
              currency
              quantity
            }
            sellers(authorizedOnly: false) {
              company {
                name
                homepageUrl
              }
              isAuthorized
              offers {
                clickUrl
                inventoryLevel
                moq
                packaging
              }
            }
          }
        }
      }
    }
    """

    result = NexarApi.query(query, %{})
    assert is_tuple(result)
    assert elem(result, 0) == :ok
  end

  test "query works w/ variables" do
    query = """
      query PartSearch($q: String!, $limit: Int!) {
      supSearchMpn(q: $q, limit: $limit){
      results{
        part{
          id
          name
          mpn
          genericMpn
          manufacturer { id name homepageUrl isVerified isDistributorApi displayFlag slug aliases }
          manufacturerUrl
          shortDescription
          specs { displayValue units value attribute { name shortname group } }
          slug
          octopartUrl
          bestImage { url }
          bestDatasheet { url }
          totalAvail
          avgAvail
          estimatedFactoryLeadDays
          freeSampleUrl
          akaMpns
      } } } }
    """

    vars = %{limit: 1, q: "555"}

    result = NexarApi.query(query, vars)
    assert is_tuple(result)
    assert elem(result, 0) == :ok
  end
end
