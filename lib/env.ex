defmodule NexarApi.Env do
  @moduledoc """
  Fetch environment variables. Will also load any variables in a local `.env` file

  - NEXAR_APP_NAME (optional)
  - NEXAR_APP_CLIENT_ID
  - NEXAR_APP_SECRET_ID
  """
  def nexar_app_name do
    Dotenv.load()

    case System.get_env("NEXAR_APP_NAME") do
      nil -> "nexar api client"
      name -> name
    end
  end

  def nexar_app_client_id do
    Dotenv.load()

    case System.get_env("NEXAR_APP_CLIENT_ID") do
      nil -> {:error, "NEXAR_APP_CLIENT_ID env variable not set!"}
      name -> {:ok, name}
    end
  end

  def nexar_app_secret_id do
    Dotenv.load()

    case System.get_env("NEXAR_APP_SECRET_ID") do
      nil -> {:error, "NEXAR_APP_SECRET_ID env variable not set!"}
      name -> {:ok, name}
    end
  end
end
