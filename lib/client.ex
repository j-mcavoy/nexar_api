defmodule NexarApi.Client do
  alias NexarApi.{Client, Env}

  defstruct client_id: nil, secret_id: nil, app_name: "Nexar Elixir Client"

  @doc """
  creates NexarApi Client config. If no client and secret id's are passed, it will automatically search for environment variables or `.env`

  see: [NexarApi.Env](NexarApi.Env.html)
  """
  def new() do
    with {:ok, client_id} <- Env.nexar_app_client_id(),
         {:ok, secret_id} <- Env.nexar_app_secret_id(),
         do: new(client_id, secret_id)
  end

  def new(client_id, secret_id), do: %Client{client_id: client_id, secret_id: secret_id}

  def new(client_id, secret_id, app_name),
    do: %Client{client_id: client_id, secret_id: secret_id, app_name: app_name}
end
