defmodule NexarApi do
  @moduledoc """
  Documentation for `NexarApi`.
  """
  alias NexarApi.{Client, Env, Auth}

  def client() do
    with {:ok, client_id} <- Env.nexar_app_client_id(),
         {:ok, secret_id} <- Env.nexar_app_secret_id(),
         do: client(client_id, secret_id)
  end

  def client(client_id, secret_id), do: %Client{client_id: client_id, secret_id: secret_id}

  require Cachex
  @cache :api_queries
  @cache_dump_location ".api_queries.cache"
  @api_endpoint "https://api.nexar.com/graphql"

  @doc """
  loads cachex cache from disk if exists
  """
  defp load_cache() do
    Cachex.load(@cache, @cache_dump_location)
  end

  @doc """
  main function for querying the Nexar GraphQL API

  query - the GraphQL query
  vars (optional) - define variables specified in the GraphQL query see Neuron Documentation (https://hexdocs.pm/neuron/readme.html)

  each unique query is cached to avoid unnecessary API calls
  """
  def query(query, %{} = vars), do: query(client(), query, %{} = vars)

  def query(client = %Client{}, query, %{} = vars) do
    key = %{query: query, vars: vars}

    case Cachex.get(@cache, key) do
      {:error, :no_cache} ->
        Cachex.start(@cache)
        Cachex.load(@cache, @cache_dump_location)
        query(query, vars)

      {:ok, nil} ->
        Neuron.Config.set(parse_options: [keys: :atoms])

        with {:ok, resp} <-
               Neuron.query(
                 query,
                 vars,
                 url: @api_endpoint,
                 headers: [token: Auth.bearer_token(client)]
               ) do
          results = %{body: resp.body}
          {:ok, _} = Cachex.put(@cache, key, results)

          {:ok, _} = Cachex.dump(@cache, @cache_dump_location)

          {:ok, results}
        else
          {:error, e} -> {:error, e}
        end

      {:ok, results} ->
        {:ok, results}

      {:error, e} ->
        {:error, e}
    end
  end
end
