defmodule NexarApi.Auth do
  require Cachex
  require OAuth2

  alias NexarApi.Client

  @nexar_oauth2_endpoint "https://identity.nexar.com"
  @cache :nexar_oauth_token
  @bearer_cache_key :bearer_token

  def bearer_token(client = %Client{}) do
    case Cachex.get(@cache, @bearer_cache_key) do
      {:error, :no_cache} ->
        Cachex.start(@cache)
        bearer_token(client)

      {:ok, nil} ->
        {token, ttl} = fetch_bearer_token(client)
        Cachex.put(@cache, @bearer_cache_key, token, ttl: :timer.seconds(ttl))
        token

      {:ok, token} ->
        token
    end
  end

  defp oauth_client(%Client{client_id: client_id, secret_id: secret_id}) do
    OAuth2.Client.new(
      strategy: OAuth2.Strategy.ClientCredentials,
      client_id: client_id,
      client_secret: secret_id,
      authorize_url: @nexar_oauth2_endpoint,
      redirect_uri: "https://api.nexar.com/graphql",
      scope: "supply.domain",
      token_url: "https://identity.nexar.com/connect/token"
    )
  end

  defp fetch_bearer_token(client = %Client{}) do
    {:ok, resp} =
      OAuth2.Client.get_token(oauth_client(client), [], [
        {"User-Agent", client.app_name}
      ])

    %{access_token: token, expires_in: ttl} = Jason.decode!(resp.token.access_token, keys: :atoms)
    {token, ttl}
  end
end
