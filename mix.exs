defmodule NexarApi.MixProject do
  use Mix.Project

  def project do
    [
      app: :nexar_api,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:neuron, "~> 5.0.0"},
      {:jason, "~> 1.2"},
      {:oauth2, "~> 2.0"},
      {:cachex, "~> 3.4.0"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:dotenv, "~> 3.0.0"}
    ]
  end
end
